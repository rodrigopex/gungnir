#!/bin/bash

#TRON=/home/leonardo/Desktop/UPPAAL/uppaal-tron-1.5-linux/tron
#TRON=/Applications/UPPAAL.app/Contents/MacOS/JavaApplicationStub
TRON=/Applications/UPPAAL.app/Contents/MacOS/server
#${5}
#`which tron`

//modelo uppaal do isa
specModel=${1}

//modelo uppaal do ladder
codeModel=${2}

//arquivo de configura��o
confFile=${3}

//n�mero do trace
traceNumber=${4}


###############################################################################
##### GENERATING TRACES
##### TRON IN EMULATION MODE
###############################################################################

echo "Generating traces!"

echo "Generating trace output-${traceNumber}!"
yes "delay 1.0, *;" | ${TRON} -P eager -Q log -I TraceAdapter -v 9 -D /tmp/output-${traceNumber} ${specModel} -- ${confFile} -e
#> Log-${traceNumber} # /dev/null # Log-${traceNumber} 

echo "output-${traceNumber} done!"

echo "Trace generated!"

###############################################################################
##### CONVERTING TRON OUTPUT FORMAT FROM EMULATION TO MONITORING
###############################################################################

echo "Converting file!"
echo "Converting file output-${traceNumber} to output-${traceNumber}-monitoring!"

cat /tmp/output-${traceNumber} | sed -e "s/input/output/" | sed -e "s/$/;/g" | sed -e "s/delay  /delay @/g" > /tmp/output-${traceNumber}-monitoring

echo "output-${traceNumber}-monitoring done!"
echo "Convertion finished!"


###############################################################################
##### TESTING TRACES
##### TRON IN MONITORING MODE
###############################################################################

echo "Initiate tests!"

echo "Testing trace output-${traceNumber}-monitoring!"
cat /tmp/output-${traceNumber}-monitoring | ${TRON} -P eager -Q log -I TraceAdapter -v 9 -o /dev/stdout -S /dev/null ${codeModel} -- ${confFile} -m > /tmp/result-${traceNumber}-trace
	
echo "output-${traceNumber}-monitoring tested!"
echo "Creating output file result-${traceNumber}-trace!"
echo "result-${traceNumber}-trace done!"
# cat /tmp/result-${traceNumber}-trace

echo "End of tests!"
echo "Look at result-x-trace for verdict, trace and post-mortem analysis!"
