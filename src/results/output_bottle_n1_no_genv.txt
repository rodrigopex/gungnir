python2.7 gungnir.py -n 1 -i ../resource/my_bottle_isa.xml -l ../resource/my_bottle_ladder.xml -v bottle_n1_no_genv.vcd
(1, '../resource/my_bottle_isa.xml', '../resource/my_bottle_ladder.xml', 'bottle_n1_no_genv.vcd', False)

===============
output_M2
---------------
   ['input_PB1', 'feedback_M2', 'input_PB2']
0:
  0, 0, x
  x, x, 1
1:
  1, x, 0
  x, 1, 0
---------------
Covered: [0, 0, 0, 0]
===============


===============
output_M1
---------------
   ['var_timer_DI01', 'input_LS', 'feedback_M2']
0:
  0, 1, x
  0, x, 0
1:
  1, x, x
  x, 0, 1
---------------
Covered: [0, 0, 0, 0]
===============


===============
output_Sol
---------------
   ['input_PE', 'var_timer_DI02', 'feedback_M2']
0:
  1, x, x
  x, 0, x
  x, x, 0
1:
  0, 1, 1
---------------
Covered: [0, 0, 0, 0]
===============


===============
var_timer_DI01
---------------
   ['input_PE']
0:
  0
1:
  1
---------------
Covered: [0, 0]
===============


===============
var_timer_DI02
---------------
   ['input_LS']
0:
  0
1:
  1
---------------
Covered: [0, 0]
===============

Coverage:[========================================] 177 scan cycles executed.
output_M2 is full covered: True, [67, 72, 58, 59]
output_M1 is full covered: True, [78, 100, 6, 47]
output_Sol is full covered: True, [98, 167, 105, 1]
var_timer_DI01 is full covered: True, [80, 6]
var_timer_DI02 is full covered: True, [98, 11]
Tests passed!