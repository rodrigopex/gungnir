../../gungnir.py -n1 -i ../../../resource/my_bottle_isa.xml -l ../..
/../resource/my_bottle_ladder_error_not.xml -v ./error_not_n1_env.vcd -g
(1, '../../../resource/my_bottle_isa.xml', '../../../resource/my_bottle_ladder_error_not.xml', './error_not_n1_env.vcd', True)

===============
output_M2
---------------
   ['input_PB1', 'feedback_M2', 'input_PB2']
0:
  0, 0, x
  x, x, 1
1:
  1, x, 0
  x, 1, 0
---------------
Covered: [0, 0, 0, 0]
===============


===============
output_M1
---------------
   ['var_timer_DI01', 'input_LS', 'feedback_M2']
0:
  0, 1, x
  0, x, 0
1:
  1, x, x
  x, 0, 1
---------------
Covered: [0, 0, 0, 0]
===============


===============
output_Sol
---------------
   ['input_PE', 'var_timer_DI02', 'feedback_M2']
0:
  1, x, x
  x, 0, x
  x, x, 0
1:
  0, 1, 1
---------------
Covered: [0, 0, 0, 0]
===============


===============
var_timer_DI01
---------------
   ['input_PE']
0:
  0
1:
  1
---------------
Covered: [0, 0]
===============


===============
var_timer_DI02
---------------
   ['input_LS']
0:
  0
1:
  1
---------------
Covered: [0, 0]
===============

Coverage:[====================                    ] 1 scan cycles executed.
output_M2 is full covered: False, [0, 0, 2, 1]
output_M1 is full covered: False, [0, 1, 0, 1]
output_Sol is full covered: False, [0, 2, 1, 0]
var_timer_DI01 is full covered: False, [2, 0]
var_timer_DI02 is full covered: False, [2, 0]
Tests failed at rung output_M1!