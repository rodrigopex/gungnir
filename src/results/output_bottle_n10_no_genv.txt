python2.7 gungnir.py -n10 -i ../resource/my_bottle_isa.xml -l ../resource/my_bottle_ladder.xml -v ./results/bottle_n10_no_genv.vcd
(10, '../resource/my_bottle_isa.xml', '../resource/my_bottle_ladder.xml', './results/bottle_n10_no_genv.vcd', False)

===============
output_M2
---------------
   ['input_PB1', 'feedback_M2', 'input_PB2']
0:
  0, 0, x
  x, x, 1
1:
  1, x, 0
  x, 1, 0
---------------
Covered: [0, 0, 0, 0]
===============


===============
output_M1
---------------
   ['var_timer_DI01', 'input_LS', 'feedback_M2']
0:
  0, 1, x
  0, x, 0
1:
  1, x, x
  x, 0, 1
---------------
Covered: [0, 0, 0, 0]
===============


===============
output_Sol
---------------
   ['input_PE', 'var_timer_DI02', 'feedback_M2']
0:
  1, x, x
  x, 0, x
  x, x, 0
1:
  0, 1, 1
---------------
Covered: [0, 0, 0, 0]
===============


===============
var_timer_DI01
---------------
   ['input_PE']
0:
  0
1:
  1
---------------
Covered: [0, 0]
===============


===============
var_timer_DI02
---------------
   ['input_LS']
0:
  0
1:
  1
---------------
Covered: [0, 0]
===============

Coverage:[========================================] 1135 scan cycles executed.
output_M2 is full covered: True, [445, 540, 282, 285]
output_M1 is full covered: True, [511, 719, 52, 205]
output_Sol is full covered: True, [570, 1055, 757, 10]
var_timer_DI01 is full covered: True, [566, 52]
var_timer_DI02 is full covered: True, [596, 81]