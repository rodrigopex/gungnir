#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
 File: timed_automata_words.py
 Author: Rodrigo Peixoto - rodrigopex@email
 Date: January 26, 2010

 Description:
 This module contains timedwword model.

 License: GPL
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 USA
'''
from types import DictType

class TimedWWord:
    '''
    This is the timewword abstraction.
    '''
    def __init__(self, timed_variables=None, alphabet=None):
        '''Constructor of the TimedWWord class

        Arguments
            -- timed_variables - is a list of tuples containg (<int> occurency time, {<str> "symbol": value}).
                               P.S.: The time occurency must be integer
            -- alphabet - is a set of possible words' alphabet symbols
        Exceptions
            -- AssertionError - it occurs if the word not be a valid word
        '''
        if timed_variables == None:
            timed_variables = {}
        elif type(timed_variables) != DictType:
            timed_variables = dict(timed_variables)
        self.timed_variables = timed_variables
        self.alphabet = alphabet
        #assert self.is_valid(), "The TimedWWord parameters are wrong, check it out."
        
    def __getitem__(self, key):
        return self.timed_variables[key]
    
    def __len__(self):
        return len(self.timed_variables)
        
    def __repr__(self):
        return "TimedWWord({0}...{1})".format("->".join(["({0},{1})".format(s, t) for s, t in sorted(self.timed_variables.items())]), self.alphabet)
        
    def __str__(self):
        return repr(self)
        
    def append(self, item):
        k, v = item
        self.timed_variables[k] = v
    
    def update(self, vars):
        self.timed_variables.update(vars)
        
    def has_key(self, key):
        return self.timed_variables.has_key(key) 
        
#    def is_valid(self):
#        try:
#            vars = set(reduce(lambda x, y: x + y, [i[1].keys() for i in self.timed_variables]))
#        except AttributeError:
#            return 0
#        return vars.issubset(set(self.alphabet))

if __name__ == "__main__":
    import random
    new_wword = []
    for i in xrange(16):
        new_wword.append({'va': random.choice([0, 1])})
    test3 = TimedWWord(dict(zip([10, 12, 15, 20, 27, 36, 50, 55, 61, 62, 68, 75, 90, 98, 101, 110], new_wword)), ['va'])
    print(test3)
    for i in range(111):
        if test3.has_key(i):
            print("time:{0} | System change: {1}".format(i, test3[i]))
    
