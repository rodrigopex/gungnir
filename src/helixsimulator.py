#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
 File: helixsimulator.py
 Author: rodrigopex - rodrigopex@email
 Date: Sep 24, 2010

 License: GPL
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 USA
'''
from helixtimedwautomata import *
from timedautomatawords import TimedWWord
from state import State, Transition
from mainfactor import *
import random
from sys import stdout


class Gungnir:
    def __init__(self, timewword, twautomatas, twautomatas_sut=None, vcd_file_path=None, mainfactor_tables=None, scancycle=None):
        self.timewword = timewword
        self.twautomatas = twautomatas
        self.twautomatas_sut = twautomatas_sut
        self.vcd_file_path = vcd_file_path
        self.mainfactor_tables = mainfactor_tables
        self.scancycle = scancycle
        self.timed_inputs = TimedWWord()
        self.timed_outputs = TimedWWord()
        self.vcd_file_path = vcd_file_path
        if self.vcd_file_path:
            self.dict_chars = {}
            self.vcd_file_handle = open(vcd_file_path, "w")
            self.vcd_file_handle.write(self.create_vcd_file())
            self.vcd_file_handle.close()
            self.vcd_file_handle = open(vcd_file_path, "a")
            self.last_inputs = self.filter_variables("input", isa=False).copy()
            self.last_outputs = self.filter_variables("output", isa=False).copy() 
    
    def filter_variables(self, keyword, isa=True):
        ret = {}
        if isa:
            for var, value in self.twautomatas[0].variables.items():
                if var.startswith(keyword):
                    ret[var] = value
        else:
            for var, value in self.twautomatas_sut[0].variables.items():
                if var.startswith(keyword):
                    ret[var] = value
        return ret
            
#    def clear_atms_candidates(self):
#        for atm in self.twautomatas:
#            atm.clear_guard_candidate()

    
    def create_vcd_file(self):
        vcd_file_head = """$date Sept 10 2008 12:00:05 $end
$version Example Simulator V0.1 $end
$timescale 100 ms $end
$scope module top $end
{0}
$upscope $end
$enddefinitions $end
{1}
"""     
        inout_names = self.filter_variables("output").keys() + \
                      self.filter_variables("input").keys()
        for i, name in enumerate(inout_names):
            self.dict_chars[name] = chr(33 + i)
            
        vars_declaration = []
        vars_init = []
        for name, char in self.dict_chars.items():
            vars_declaration.append("$var wire 1 {0} {1} $end".format(char,name))
            vars_init.append("0 {0}".format(char))
        vars_declaration = "\n".join(vars_declaration)
        vars_init = "\n".join(vars_init)
        vcd_file_head = vcd_file_head.format(vars_declaration, vars_init)
        return vcd_file_head
    
    def dump_vars(self, inputs=True):
        current_variables = self.filter_variables("input", isa=False)
        last_variables = self.last_inputs.copy()
        current_clock = self.twautomatas_sut[-1].system_clock
        if not inputs:
            current_variables = self.filter_variables("output", isa=False)
            last_variables = self.last_outputs.copy()
            current_clock = self.twautomatas_sut[-1].system_clock + 1            
            
            #if current_variables['output_B'] == 1:
            #    print "Aquiiiiiiiiiiiii!"

        updated_variables = []
        
        for k,v in current_variables.items():
            try:
                if v != last_variables[k]:
                    updated_variables.append("{0} {1}".format(int(v), self.dict_chars[k]))
            except KeyError:
                print "KeyError:", k
        if updated_variables:
            dump_string = "#{0}\n{1}\n".format(current_clock, "\n".join(updated_variables))
            self.vcd_file_handle.write(dump_string)
            
        
        if inputs:
            self.last_inputs = current_variables.copy()
        else:
            self.last_outputs = current_variables.copy()

    def check_coverage(self):
        all_covered = 0
        for mt in self.mainfactor_tables:
            mt.match_vector(self.twautomatas[0].variables)
            all_covered+=mt.count_covered(1)
        return all_covered
    
    def progress_bar(self, current, total, count_tests):
        bar = current*40/total
        stdout.flush()
        stdout.write("\rCoverage:[{1:<40}] {0} scan cycles executed.".format(count_tests, "="*bar))
        
    def generate_random_inputs_tests(self, sysclock):
        inputs = self.filter_variables("input").copy()
        
        for k in inputs:
            inputs[k] = random.randrange(2)
            
        for atm in self.twautomatas:
            if atm.name == "ReadInputs":
                atm.timedwword.append((sysclock, inputs))
                break
        for atm in self.twautomatas_sut:
            if atm.name == "ReadInputs":
                atm.timedwword.append((sysclock, inputs))
                break            

    def generate_environment_based_input_tests(self, level=1):
        #inputs = self.filter_variables("input").copy()
        inputs = [(0, {'input_PB2': 0, 'input_PB1': 0, 'input_LS': 0, 'input_PE': 0}),
                  (1, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (2, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (3, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (4, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (5, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (6, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (7, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (8, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (9, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (10, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (11, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (12, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (13, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (14, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (15, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (16, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (17, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (18, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (19, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (20, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (21, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (22, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (23, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (24, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (25, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (26, {'input_PB2': 1, 'input_PB1': 0, 'input_LS': 0, 'input_PE': 0}),
                  (27, {'input_PB2': 1, 'input_PB1': 0, 'input_LS': 0, 'input_PE': 0}),
                  (28, {'input_PB2': 1, 'input_PB1': 0, 'input_LS': 0, 'input_PE': 0})]
        
        inputs = [(0, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (1, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (2, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (3, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (4, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (5, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (6, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (7, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (8, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (9, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (10, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (11, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (12, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (13, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (14, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (15, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (16, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (17, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (18, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (19, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (20, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 1}),
                  (21, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 1, 'input_PE': 0}),
                  (22, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (23, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (24, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (25, {'input_PB2': 0, 'input_PB1': 1, 'input_LS': 0, 'input_PE': 0}),
                  (26, {'input_PB2': 1, 'input_PB1': 0, 'input_LS': 0, 'input_PE': 0}),
                  (27, {'input_PB2': 1, 'input_PB1': 0, 'input_LS': 0, 'input_PE': 0}),
                  (28, {'input_PB2': 1, 'input_PB1': 0, 'input_LS': 0, 'input_PE': 0})]
        new_inputs = []
        for i in range(level):
            for t, v in inputs:
                new_inputs.append((t + i*29,v))
        #print new_inputs
        inputs = new_inputs
        res = i*29 or 28

        for atm in self.twautomatas:
            if atm.name == "ReadInputs":
                atm.timedwword.update(inputs)
                break
        for atm in self.twautomatas_sut:
            if atm.name == "ReadInputs":
                atm.timedwword.update(inputs)
                break  
        return res
        
    def generate_online_random_test_cases(self):
        system_clock = self.generate_environment_based_input_tests()
        error = 0
        total_coverage = 0
        fault_rung = None
        for i in self.mainfactor_tables:
            total_coverage+=i.coverable_count()
        try:    
            for i in xrange(10000):
                self.generate_random_inputs_tests(system_clock)
                while 1:
                    for atm in self.twautomatas:
                        atm.list_candidates()
                    
                    chosen_atm = None
                    chosen_transition = None
                    
                    random.shuffle(self.twautomatas)
                    for chosen_atm in self.twautomatas:
                        if chosen_atm.guards_candidate:
                            chosen_transition = chosen_atm.choose_candidate()
                            break
                    else:
                        raise Exception("DeadLock!")
                    if chosen_atm.current_state.committed:
                        pass
                    else:
                        atm = self.twautomatas[-1]
                        #self.timed_inputs.append((atm.system_clock, self.filter_variables("input")))
                        atm.update_clocks()
                        system_clock+=self.scancycle
                        #self.timed_outputs.append((atm.system_clock, self.filter_variables("output")))
                        break
                
                current_coverage = self.check_coverage()
                if current_coverage == total_coverage:
                    stdout.flush()
                    stdout.write("\rCoverage:[{1}] {0} scan cycles executed.\n".format(i, "="*40))
                    break
                else:
                    self.progress_bar(current_coverage, total_coverage, i)
                    
                if self.vcd_file_path:
                    self.dump_vars(inputs=True)
                #print self.twautomatas[-1].variables
     
                while 1:
                       for atm in self.twautomatas_sut:
                           atm.list_candidates()
                       
                       chosen_atm = None
                       chosen_transition = None
                       
                       random.shuffle(self.twautomatas_sut)
                       for chosen_atm in self.twautomatas_sut:
                           if chosen_atm.guards_candidate:
                               chosen_transition = chosen_atm.choose_candidate()
                               break
                       else:
                           raise Exception("DeadLock!")
                       if chosen_atm.current_state.committed:
                           pass
                       else:
                           atm = self.twautomatas_sut[-1]
                           #self.timed_inputs.append((atm.system_clock, self.filter_variables("input")))
                           atm.update_clocks()
                           system_clock+=self.scancycle
                           #self.timed_outputs.append((atm.system_clock, self.filter_variables("output")))
                           break
                #print self.twautomatas_sut[0].variables
                fault_rung = self.check_consistency()
                if fault_rung:
                   error = fault_rung
                   stdout.write("\n")
                   break
        except KeyboardInterrupt:
            stdout.write("\n")
        for mt in self.mainfactor_tables:
            print "{0} is full covered: {1}, {2}".format(mt.output_name, mt.is_full_covered(), mt.covered)
        return i+1, error

    
    def check_consistency(self):
        ret = 0
        variables_isa = self.filter_variables("output")
        variables_ladder = self.filter_variables("output", isa=False)
        #error = self.filter_variables("output") != self.filter_variables("output", isa=False)
        error = variables_isa != variables_ladder
        if error:
            for k,v in variables_isa.items():
                if variables_ladder[k] != v:
                    ret = k
                    break
            ret = k
        if self.vcd_file_path:
            self.dump_vars(inputs=False)
        return ret
            

def verify(isa_file, ladder_file, scan_cycle=None, vcd_file_path=None):
    tww = TimedWWord()
    
    from modelgenerator import ModelGeneratorISA52, ModelGeneratorLadder
    
    mgi = ModelGeneratorISA52()
    automatas = mgi.digest(isa_file)
    automatas[0].__class__.clock_granularity = scan_cycle
    mainfactor_extractor = MainFactorExtractor()
    mainfactor_tables = mainfactor_extractor.digest(isa_file)
    for i in automatas:
        print i
    
    for i in mainfactor_tables:
        print i
    
    mgl = ModelGeneratorLadder()
    automatas_sut = mgl.digest(ladder_file)
    automatas_sut[0].__class__.clock_granularity = scan_cycle
    
    test = Gungnir(tww, automatas, twautomatas_sut=automatas_sut, 
                   vcd_file_path=vcd_file_path,
                   mainfactor_tables=mainfactor_tables, scancycle=scan_cycle)
    
    return test.generate_online_random_test_cases()


if __name__ == '__main__':

#######################################   
#Testes para a Engarrafadora! 
#######################################
    
    times, error = verify("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/my_bottle_isa.xml",
                          "/Users/rodrigopex/Msc/dissertacao/msc/code/resource/my_bottle_ladder.xml",
                          1, "/Users/rodrigopex/Desktop/bottle.vcd")
    
    if error:
        print("Tests failed at rung {0}!".format(error))
    else:
        print("Tests passed!")
        
        
#    error_count = 0
#    avarage = 0
#    error = True
#    for i in range(1000):
#        times, error = verify("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/my_bottle_isa.xml",
#                          "/Users/rodrigopex/Msc/dissertacao/msc/code/resource/my_bottle_ladder_error_not.xml",
#                          1, "/Users/rodrigopex/Desktop/bottle.vcd")
#        if error:
#            error_count+=1
#        else:
#            print "Problema na no i =", i
#        avarage+=times
#    print error_count
#    print avarage/1000.0
#        
#    times, error = verify("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/timer_test01_isa.xml",
#                          "/Users/rodrigopex/Msc/dissertacao/msc/code/resource/timer_test01_ladder.xml",
#                          1, "/Users/rodrigopex/Desktop/bottle.vcd")
#    
#    if error:
#        print("Tests failed at rung {0}!".format(error))
#    else:
#        print("Tests passed!")
    
#    error_count = 0
#    avarage = 0
#    for i in range(2000):
#        times, error = verify("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/timer_test01_isa.xml",
#                          "/Users/rodrigopex/Msc/dissertacao/msc/code/resource/timer_test01_ladder.xml",
#                          1, "/Users/rodrigopex/Desktop/bottle.vcd")
#        if error:
#            error_count+=1
#        avarage+=times
#    print error_count
#    print avarage/2000.0
