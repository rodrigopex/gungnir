#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
 File: checker.py
 Author: rodrigopex - rodrigopex@email
 Date: Aug 25, 2010

 License: GPL
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 USA
'''
import loader
from xsimulator import Gungnir
from timedautomatawords import TimedWWord

if __name__ == '__main__':
    res = loader.load_system("/Users/rodrigopex/Msc/dissertacao/msc/code/doc/uppaal_examples/xsimulator_example_sync3.xml")
    test = Gungnir(None, res)
    for i in res:
        print i
    test.simulate(100)

