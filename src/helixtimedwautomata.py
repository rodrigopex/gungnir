#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
 File: timedwautomata.py
 Author: rodrigopex - rodrigopex@email
 Date: Jun 22, 2010

 License: GPL
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 USA
'''
from timedautomatawords import TimedWWord
from state import State, Transition
from types import ListType, StringType
import random
import re


#class AutomataBaseModel(object):
#    #variables = {}
#    def __init__(self, states, intial_state, variables, variables_defs=None):
#        
#    
#    
#
#                        
#class TimedAutomataBaseModel(AutomataBaseModel):
#    def __init__(self, states, intial_state, variables, variables_defs=None):
#        AutomataBaseModel.__init__(self, states, intial_state, variables, variables_defs)
#    

class TimedAutomata:
    #variables = {}
    def __init__(self, states, intial_state, variables, timedwword=None, name=None, variables_defs=None):
        #TimedAutomataBaseModel.__init__(self, states, intial_state, variables, variables_defs=None)
        # Static variables assignment. 
        
        self.set_variables(variables)
        self.__class__.system_clock = 0
        self.__class__.clock_granularity = 1 
        #-----------------------------
        self.states = states
        self.intial_state = intial_state
        self.current_state = intial_state
        self.variables_defs = variables_defs
        
        assert not self.check_definition(), "Definition error!"
        self.name = name or "no_name"
        self.timedwword = timedwword
        self.waiting_for = set() #waiting for channel sync
        self.emit = set()
        self.guards_candidate = []
        self.will_synchronize = False
        self.id = -1
    
    def __cmp__(self, other):
        ret = 0
        if self.name > other.name:
            ret = 1
        elif self.name < other.name:
            ret = -1
        return ret
    
    def __repr__(self):
        ret = "Automata: {0}\n|\n{1}"
        return ret.format(self.name, "\n".join([repr(s) for s in self.states]))
    
    def set_variables(self, variables):
        self.__class__.variables.update(variables)

    def check_definition(self):
        pass
    
    def update_clocks(self):
        clocks = filter(lambda x: x.startswith('clock_'), self.variables)
        for clk in clocks:
            self.__class__.variables[clk] += self.__class__.clock_granularity
        self.__class__.system_clock += self.__class__.clock_granularity
    
    def update_variables(self, **timed_symbol):
        '''
        Update variables.
        '''
        for k, v in timed_symbol.items():
            if self.variables.has_key(k):
                if type(v) == StringType:
                    self.__class__.variables[k] = eval(v, {}, self.__class__.variables)
                else:
                    self.__class__.variables[k] = v
            else:
                print(timed_symbol)
                raise NameError("Gungnir warning: Variable {0} is not defined!".format(k))
    
    def restore_all(self):
        self.current_state = self.intial_state
        self.__class__.system_clock = 0
        for i in self.variables:
            if i.startswith('clock_'):
                self.__class__.variables[i] = 0
            else:
                self.__class__.variables[i] = None
    
    def add_state(self, p_state):
        self.states.append(p_state)
        
    def set_initial_state(self, p_state):
        self.intial_state = p_state
    
    def get_state_by_name(self, p_name):
        ret = None
        for state in self.states:
            if state.name == p_name:
                ret = state
                break
        return ret
    
    def has_guard_candidate(self):
        return bool(self.guards_candidate)
    
    def add_guard_candidate(self, trans):
        if trans:
            self.guards_candidate.append(trans)
        else:
            print trans
            raise ValueError("Transition must be a Transition object instance!")
        
    def clear_guard_candidate(self):
        self.guards_candidate = []
    
    def list_candidates(self):
        '''
        This method represents the symbols automata consuming action. At this action
        the automata lives. This method is written in C style to improve performance.
        It is a little bit less pythonic.
        '''
        #system_changed = 0
        
        if self.timedwword and self.timedwword.has_key(self.system_clock):
            self.update_variables(**self.timedwword[self.system_clock])
            #system_changed = 1
           
        current_expression = self.current_state.transitions 
        
        for scur_exp in current_expression:
            #guards, assignments, sync, target_state = (scur_exp['guards'],
            guards, assignments, target_state = (scur_exp['guards'],                                                       
                                                 scur_exp['assignments'],
                                                 #scur_exp['sync'],
                                                 scur_exp['target_state'])
    
            if guards != None:
                if eval(guards, {}, self.__class__.variables):
                    # guards are valid!
                    pass
                else:
                    continue
            self.add_guard_candidate(scur_exp)   
            
    def choose_candidate(self,index=None):
        if self.has_guard_candidate():
            chosen_transition = None
            if index:
                chosen_transition = self.current_state.transitions[index]
            else:
                chosen_transition = random.choice(self.guards_candidate)
            self.current_state = chosen_transition['target_state']
            self.update_variables(**chosen_transition['assignments'])
            self.clear_guard_candidate()
 
class TimedAutomata_ISA(TimedAutomata):
    variables = {}

class TimedAutomata_Ladder(TimedAutomata):
    variables = {}
 
if __name__ == "__main__":
    variables = {'va': None, 'clock_x':0, 'clock_y':0}
    s0 = State("S0")
    s1 = State("S1")
    s2 = State("S2")
    states = [s0, s1, s2]
    
    s0.add_transition(Transition(guards="va == 0",
                                 assignments={'clock_x': 0, 'clock_y': 0},
                                 target_state=s1,
                                 sync=None))
    s1.add_transition(Transition(guards="clock_x < 20 and va == 1",
                                 assignments={'clock_y': 0},
                                 target_state=s2,
                                 sync=None))
    s2.add_transition(Transition(guards="clock_y < 20 and va == 0",
                                 assignments={'clock_x': 0},
                                 target_state=s1,
                                 sync=None))
                
    initial_state = s0
    ta = TimedAutomata_ISA(states, initial_state, variables)
    tww = TimedWWord(zip([10, 13, 22, 48, 53], [{'va':0}, {'va':1}, {'va':0}, {'va':1}, {'va':0}]), ['va'])
    ta.timedwword = tww
    clock_state = {5: "S0", 10: "S1", 13:"S2", 22:"S1", 48:"S1", 53:"S1"}
    print(ta)
    for i in xrange(60):
        ta.update_clocks()
        ta.list_candidates()
        ta.choose_candidate()
        if i in clock_state.keys():
            print ta.current_state.name, clock_state[i]
            assert ta.current_state.name == clock_state[i]
            
    
    ta.restore_all()
    tww = TimedWWord(zip([10, 13, 22, 38], [{'va':0}, {'va':1}, {'va':0}, {'va':1}]), ['va'])
    ta.timedwword = tww
    clock_state = {5: "S0", 10: "S1", 13:"S2", 22:"S1", 38:"S2"}
    for i in xrange(40):
        ta.update_clocks()
        ta.list_candidates()
        ta.choose_candidate()
        if i in clock_state.keys():
            print ta.current_state.name, clock_state[i]
            assert ta.current_state.name == clock_state[i]
#                print j, local_ta.current_state.name, clock_state[j]
#                if local_ta.current_state.name != clock_state[j]:
#                    print "AQU!!!!"
    
#    variables = {'va': 0, 'clock_x':0, 'clock_y':0}
#        
#    s0 = State("S0")
#    s1 = State("S1")
#    s2 = State("S2")
#    states = [s0, s1, s2]
#    
#    s0.add_transition(Transition(guards="va == 0",
#                                 assignments={'clock_x': 0, 'clock_y': 0},
#                                 target_state=s1,
#                                 sync=None))
#    s1.add_transition(Transition(guards="clock_x < 20 and va == 1",
#                                 assignments={'clock_y': 0},
#                                 target_state=s2,
#                                 sync=None))#"a!"))
#    s2.add_transition(Transition(guards="clock_y < 20 and va == 0",
#                                 assignments={'clock_x': 0},
#                                 target_state=s1,
#                                 sync=None))#"b?"))
#        
#    initial_state = s0
#    tww = TimedWWord(zip([10, 21, 22, 38, 43], [{'va':0}, {'va':1}, {'va':0}, {'va':1}, {'va':0}]), ['va'])
#    ta = TimedAutomata(states, initial_state, variables, timedwword=tww)
#
#    for i in xrange(400):
#        #ta.update_clocks()
#        error = ta.list_candidates()
#        if error:
#            print("Deu merda!")
#            break
#        else:
#            ta.choose_candidate()
