#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
 File: modelgenerator.py
 Author: rodrigopex - rodrigopex@email
 Date: Sep 22, 2010

 License: GPL
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 USA
'''
from timedautomatawords import TimedWWord
from state import State, Transition
from helixtimedwautomata import TimedAutomata_ISA, TimedAutomata_Ladder
from xml.etree import ElementTree


class ModelGeneratorISA52:
    def __init__(self):
        self.variables = {'id': 0}
        self.automata_network = {}
        self.automata_network['TIMERS'] = []
        self.current_id = 0
        self.rungs = {}
        self.create_READINPUTS()
        self.create_WRITEOUTPUTS()
        
    
    def create_READINPUTS(self):        
#        s0 = State("S0", committed=True)
#        states = [s0]                    
#        initial_state = s0
#        self.automata_network['READINPUTS'] = TimedAutomata_ISA(states,
#                                                            initial_state,
#                                                            self.variables,
#                                                            name="ReadInputs")
        s0 = State("S0", committed=True)
        states = [s0]                    
        initial_state = s0
        s0.add_transition(Transition(guards="id == {0}".format(self.current_id),
                                     assignments={'id': "id + 1"},#, input_name: 0},
                                     target_state=s0,
                                     sync=None))
        self.automata_network['READINPUTS'] = TimedAutomata_ISA(states,
                                                            initial_state,
                                                            self.variables,
                                                            name="ReadInputs",
                                                            timedwword=TimedWWord())
        self.current_id+=1
    def add_input_trasition(self, input_name):
        state = self.automata_network['READINPUTS'].states[0]
        state.add_transition(Transition(guards="id == {0}".format(self.current_id),
                                     assignments={'id': "id + 1", input_name: 0},
                                     target_state=state,
                                     sync=None))
        state.add_transition(Transition(guards="id == {0}".format(self.current_id),
                                     assignments={'id': "id + 1", input_name: 1},
                                     target_state=state,
                                     sync=None))
        self.current_id+=1
    
    def add_feedback_trasition(self, feedback_name):
        state = self.automata_network['READINPUTS'].states[0]
        output_name = feedback_name
        output_name
        state.add_transition(Transition(guards="id == {0}".format(self.current_id),
                                     assignments={feedback_name: feedback_name.replace("feedback", "output"),
                                                  'id': "id + 1"},
                                     target_state=state,
                                     sync=None))
        self.current_id+=1
    
    def create_timer(self, var_name, delay, exp):
        timer_name = var_name.strip("var_")
        self.variables[var_name] = 0
        clock_name = "clock_{0}".format(timer_name)
        self.variables[clock_name] = 0
        s0 = State("S0", committed=True)
        s1 = State("S1", committed=True)
        s2 = State("S2", committed=True)
        states = [s0, s1, s2]             
        initial_state = s0
        
        s0.add_transition(Transition(guards="id == {0} and {1} == 0".format(self.current_id, exp),
                                     assignments={var_name: 0,'id': "id + 1"},
                                     target_state=s0,
                                     sync=None))
        s0.add_transition(Transition(guards="id == {0} and {1} == 1".format(self.current_id, exp),
                                     assignments={'id': "id + 1", clock_name: 0},
                                     target_state=s1,
                                     sync=None))
        s1.add_transition(Transition(guards="id == {0} and {1} == 1 and {2} < {3}".format(self.current_id, exp, clock_name, delay),
                                     assignments={'id': "id + 1"},
                                     target_state=s1,
                                     sync=None))
        s1.add_transition(Transition(guards="id == {0} and {1} == 0".format(self.current_id, exp),
                                     assignments={'id': "id + 1"},
                                     target_state=s0,
                                     sync=None))
        s1.add_transition(Transition(guards="id == {0} and {1} == 1 and {2} >= {3}".format(self.current_id, exp, clock_name, delay),
                                     assignments={'id': "id + 1", var_name: 1},
                                     target_state=s2,
                                     sync=None))
        s2.add_transition(Transition(guards="id == {0} and {1} == 1".format(self.current_id, exp),
                                     assignments={'id': "id + 1"},
                                     target_state=s2,
                                     sync=None))
        s2.add_transition(Transition(guards="id == {0} and {1} == 0".format(self.current_id, exp),
                                     assignments={'id': "id + 1", var_name: 0},
                                     target_state=s0,
                                     sync=None))
        
        self.automata_network['TIMERS'].append(TimedAutomata_ISA(states,
                                                              initial_state,
                                                              self.variables,
                                                              name=timer_name))
        self.current_id += 1
    
    def create_WRITEOUTPUTS(self):
        s0 = State("S0", committed=True)
        states = [s0]             
        initial_state = s0
        self.automata_network['WRITEOUTPUTS'] = TimedAutomata_ISA(states,
                                                              initial_state,
                                                              self.variables,
                                                              name="WriteOutputs")
    def add_output_trasition(self, output_name, expression):
        state = self.automata_network['WRITEOUTPUTS'].states[0]
        state.add_transition(Transition(guards="id == {0}".format(self.current_id),
                                     assignments={'id': "id + 1", output_name: expression},
                                     target_state=state,
                                     sync=None))
        self.current_id+=1

    
    def create_UPDATER(self):
        s0 = State("S0")
        states = [s0]                    
        initial_state = s0
        s0.add_transition(Transition(guards="id == {0}".format(self.current_id),
                                     assignments={'id': 0},
                                     target_state=s0,
                                     sync=None))
        ta = TimedAutomata_ISA(states, initial_state,
                           self.variables,
                           name="Updater")
        ta.id = self.current_id
        self.automata_network['UPDATER'] = ta
    def load_timer(self, var_name, delay, child):
        expression = self.load_expression(child)
        self.create_timer(var_name, delay, expression)
    
    def load_expression(self, exp):
        expression = ""
        for child in exp.getchildren():
            if child.tag == "ref_input":
                expression = "input_{0}".format(child.attrib["name"])
            elif child.tag == "ref_output":
                expression = "output_{0}".format(child.attrib["name"])
            elif child.tag == "ref_feedback":
                expression = "feedback_{0}".format(child.attrib["name"])
            elif child.tag.startswith("timer"):
                expression = "var_{0}{1}".format(child.tag, child.attrib["id"])
                self.load_timer(expression, child.attrib["delay"], child.getchildren()[0])
            elif child.tag in ["and", "or"]:
                expression = "({0} {2} {1})".format(self.load_expression(child.getchildren()[0]),
                                                    self.load_expression(child.getchildren()[1]),
                                                    child.tag)
            elif child.tag == "not":
                expression = "(not {0})".format(self.load_expression(child.getchildren()[0]))
            
        return expression
    
    def load_rung(self, rung):
        output = ""
        expression = ""
        for child in rung.getchildren():
            if child.tag == "result": 
                output = "output_{0}".format(child.getchildren()[0].attrib['name'])
            elif child.tag == "op":
                expression = self.load_expression(child)
        self.rungs[output] = expression
    
    def digest(self, xmlfile):
        root = ElementTree.parse(xmlfile).getroot()
        for child in root.getchildren():
            if child.tag == "input":
                input_name = "input_{0}".format(child.text)
                self.variables[input_name] = 0
                #self.add_input_trasition(input_name)
            elif child.tag == "feedback":
                feedback_name = "feedback_{0}".format(child.text)
                self.variables[feedback_name] = 0
                self.add_feedback_trasition(feedback_name)
            elif child.tag == "output":
                output_name = "output_{0}".format(child.text)
                self.variables[output_name] = 0
            elif child.tag == "rung":
                self.load_rung(child)
                #print self.rungs
        #print self.variables
        for output_name, expression in self.rungs.items():
            self.add_output_trasition(output_name, expression)
        self.create_UPDATER()
        ret = [self.automata_network['READINPUTS'],
               self.automata_network['WRITEOUTPUTS'],
               self.automata_network['UPDATER']]
        ret.extend(self.automata_network['TIMERS'])
        #for i in self.automata_network.values():
        #    print i
        return ret
    
class ModelGeneratorLadder:
    def __init__(self):
        self.variables = {'id': 0}
        self.automata_network = {}
        self.automata_network['TIMERS'] = []
        self.current_id = 0
        self.rungs = {}
        self.create_READINPUTS()
        self.create_WRITEOUTPUTS()
        
    
    def create_READINPUTS(self):        
        s0 = State("S0", committed=True)
        states = [s0]                    
        initial_state = s0
        s0.add_transition(Transition(guards="id == {0}".format(self.current_id),
                                     assignments={'id': "id + 1"},#, input_name: 0},
                                     target_state=s0,
                                     sync=None))
        self.automata_network['READINPUTS'] = TimedAutomata_Ladder(states,
                                                            initial_state,
                                                            self.variables,
                                                            name="ReadInputs",
                                                            timedwword=TimedWWord())
        self.current_id+=1
        
#    def add_input_trasition(self, input_name):
#        state = self.automata_network['READINPUTS'].states[0]
#        state.add_transition(Transition(guards="id == {0}".format(self.current_id),
#                                     assignments={'id': "id + 1"},#, input_name: 0},
#                                     target_state=state,
#                                     sync=None))
##        state.add_transition(Transition(guards="id == {0}".format(self.current_id),
##                                     assignments={'id': "id + 1", input_name: 1},
##                                     target_state=state,
##                                     sync=None))
#        self.current_id+=1
    
    def add_feedback_trasition(self, feedback_name):
        state = self.automata_network['READINPUTS'].states[0]
        output_name = feedback_name
        output_name
        state.add_transition(Transition(guards="id == {0}".format(self.current_id),
                                     assignments={feedback_name: feedback_name.replace("feedback", "output"),
                                                  'id': "id + 1"},
                                     target_state=state,
                                     sync=None))
        self.current_id+=1
    
    def create_timer(self, var_name, delay, exp):
        timer_name = var_name.strip("var_")
        self.variables[var_name] = 0
        clock_name = "clock_{0}".format(timer_name)
        self.variables[clock_name] = 0
        s0 = State("S0", committed=True)
        s1 = State("S1", committed=True)
        s2 = State("S2", committed=True)
        states = [s0, s1, s2]             
        initial_state = s0
        
        s0.add_transition(Transition(guards="id == {0} and {1} == 0".format(self.current_id, exp),
                                     assignments={var_name: 0,'id': "id + 1"},
                                     target_state=s0,
                                     sync=None))
        s0.add_transition(Transition(guards="id == {0} and {1} == 1".format(self.current_id, exp),
                                     assignments={'id': "id + 1", clock_name: 0},
                                     target_state=s1,
                                     sync=None))
        s1.add_transition(Transition(guards="id == {0} and {1} == 1 and {2} < {3}".format(self.current_id, exp, clock_name, delay),
                                     assignments={'id': "id + 1"},
                                     target_state=s1,
                                     sync=None))
        s1.add_transition(Transition(guards="id == {0} and {1} == 0".format(self.current_id, exp),
                                     assignments={'id': "id + 1"},
                                     target_state=s0,
                                     sync=None))
        s1.add_transition(Transition(guards="id == {0} and {1} == 1 and {2} >= {3}".format(self.current_id, exp, clock_name, delay),
                                     assignments={'id': "id + 1", var_name: 1},
                                     target_state=s2,
                                     sync=None))
        s2.add_transition(Transition(guards="id == {0} and {1} == 1".format(self.current_id, exp),
                                     assignments={'id': "id + 1"},
                                     target_state=s2,
                                     sync=None))
        s2.add_transition(Transition(guards="id == {0} and {1} == 0".format(self.current_id, exp),
                                     assignments={'id': "id + 1", var_name: 0},
                                     target_state=s0,
                                     sync=None))
        
        self.automata_network['TIMERS'].append(TimedAutomata_Ladder(states,
                                                              initial_state,
                                                              self.variables,
                                                              name=timer_name))
        self.current_id += 1
    
    def create_WRITEOUTPUTS(self):
        s0 = State("S0", committed=True)
        states = [s0]             
        initial_state = s0
        self.automata_network['WRITEOUTPUTS'] = TimedAutomata_Ladder(states,
                                                              initial_state,
                                                              self.variables,
                                                              name="WriteOutputs")
    def add_output_trasition(self, output_name, expression):
        state = self.automata_network['WRITEOUTPUTS'].states[0]
        state.add_transition(Transition(guards="id == {0}".format(self.current_id),
                                     assignments={'id': "id + 1", output_name: expression},
                                     target_state=state,
                                     sync=None))
        self.current_id+=1

    
    def create_UPDATER(self):
        s0 = State("S0")
        states = [s0]                    
        initial_state = s0
        s0.add_transition(Transition(guards="id == {0}".format(self.current_id),
                                     assignments={'id': 0},
                                     target_state=s0,
                                     sync=None))
        ta = TimedAutomata_Ladder(states, initial_state,
                           self.variables,
                           name="Updater")
        ta.id = self.current_id
        self.automata_network['UPDATER'] = ta
        
    def load_timer(self, var_name, delay, child):
        expression = self.load_expression(child)
        self.create_timer(var_name, delay, expression)
    
    def load_expression(self, exp):
        expression = ""
        for child in exp.getchildren():
            if child.tag == "ref_input":
                expression = "input_{0}".format(child.attrib["name"])
            elif child.tag == "ref_output":
                expression = "output_{0}".format(child.attrib["name"])
            elif child.tag == "ref_feedback":
                expression = "feedback_{0}".format(child.attrib["name"])
            elif child.tag.startswith("timer"):
                expression = "var_{0}{1}".format(child.tag, child.attrib["id"])
                self.load_timer(expression, child.attrib["delay"], child.getchildren()[0])
            elif child.tag in ["and", "or"]:
                expression = "({0} {2} {1})".format(self.load_expression(child.getchildren()[0]),
                                                    self.load_expression(child.getchildren()[1]),
                                                    child.tag)
            elif child.tag == "not":
                expression = "(not {0})".format(self.load_expression(child.getchildren()[0]))
            
        return expression
    
    def load_rung(self, rung):
        output = ""
        expression = ""
        for child in rung.getchildren():
            if child.tag == "result": 
                output = "output_{0}".format(child.getchildren()[0].attrib['name'])
            elif child.tag == "op":
                expression = self.load_expression(child)
        self.rungs[output] = expression
    
    def digest(self, xmlfile):
        root = ElementTree.parse(xmlfile).getroot()
        for child in root.getchildren():
            if child.tag == "input":
                input_name = "input_{0}".format(child.text)
                self.variables[input_name] = 0
                #self.add_input_trasition(input_name)
            elif child.tag == "feedback":
                feedback_name = "feedback_{0}".format(child.text)
                self.variables[feedback_name] = 0
                self.add_feedback_trasition(feedback_name)
            elif child.tag == "output":
                output_name = "output_{0}".format(child.text)
                self.variables[output_name] = 0
            elif child.tag == "rung":
                self.load_rung(child)
                #print self.rungs
        #print self.variables
        for output_name, expression in self.rungs.items():
            self.add_output_trasition(output_name, expression)
        self.create_UPDATER()
        ret = [self.automata_network['READINPUTS'],
               self.automata_network['WRITEOUTPUTS'],
               self.automata_network['UPDATER']]
        ret.extend(self.automata_network['TIMERS'])
        #for i in self.automata_network.values():
        #    print i
        return ret  
        

if __name__ == '__main__':
    test = ModelGeneratorLadder()
    #for at in test.digest("partida_motor_isa52.xml"):
    #    print at
    for at in test.digest("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/bottle_ladder.xml"):
        print at