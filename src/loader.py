#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
 File: modelgenerator.py
 Author: rodrigopex - rodrigopex@email
 Date: Jun 19, 2010

 License: GPL
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 USA
'''
from state import State, Transition
from timedwautomata import TimedAutomata
from xml.etree import ElementTree
import re

def get_state(name, state_list):
    ret = None
    for i in state_list:
        if i.name == name:
            ret = i
    return ret

def load_location(location):
    name = location.attrib["id"]
    committed = False
    for child in location.getchildren():
        if child.tag == "committed":
            committed = True
        else:
            pass
    return State(name, None, committed)
        

def refine_assignment(assignment_str):
    """
    >>> refine_assignment("PE = 0, numInputRead = 1")
    {'numInputRead': 1, 'PE': 0}
    >>> refine_assignment("test = test2")
    {'test': 'test2'}
    """
    try:
        ret = {}
        if assignment_str:
            l = assignment_str.split(",")
            for i in l:
                k, v = "", ""
                partial = i.split("=")
                if len(partial) == 1:
                    partial = partial[-1]
                    if "--" in partial:
                        partial = partial.strip("--")
                        k, v = partial, "{0} - 1".format(partial)
                    elif "++" in partial:
                        partial = partial.strip("++")
                        k, v = partial, "{0} + 1".format(partial)
                    ret[k.strip()] = v
                elif len(partial) == 2:
                    k, v = partial
                    try:
                        ret[k.strip()] = int(v)
                    except ValueError:
                        ret[k.strip()] = v.strip()
        return ret
    except:
        import pdb
        import sys
        pdb.post_mortem(sys.exc_info()[2]) 

def load_transition(node, states=None):
    """
    It add the transitions for the template. Returns it is not necessary.
    """
    source = None
    target = None
    guards = None
    sync = None
    assignments = None
    for child in node.getchildren():
        if child.tag == "source":
            source = child.attrib["ref"]
        elif child.tag == "target":
            target = child.attrib["ref"]
        elif child.tag == "label":
            kind = child.attrib["kind"]
            if kind == "guard":
                guards = child.text
            elif kind == "synchronisation": 
                sync = child.text
            elif kind == "assignment":
                assignments = child.text
            else:
                raise Exception("Malformed xml look at {0}".formta(node.tag))
    source_state = get_state(source, states)
    target_state = get_state(target, states)
    source_state.add_transition(Transition(guards=guards,
                                           assignments=refine_assignment(assignments),
                                           target_state=target_state,
                                           sync=sync))
 
def load_template(node):
    name = "NoName"
    states = []
    initial_state = None
    variables = {}
    for child in node.getchildren():
        if child.tag == "name":
            name = child.text
        elif child.tag == "location":
            #states.append(State(child.attrib["id"]))
            states.append(load_location(child))
            
        elif child.tag == "init":
            id_initial_state = child.attrib["ref"]
            for st in states:
                if st.name == id_initial_state:
                    initial_state = st
                    break
        elif child.tag == "transition":
            load_transition(child, states)
        elif child.tag == "declaration":
            print("Warning: declaration found but not handled.")
            
    return TimedAutomata(states, initial_state, variables, None, name)
                     
    

def load_system(xmlfile):
    root = ElementTree.parse(xmlfile).getroot()
    automata_network = []
    variables = {}
    for child in root.getchildren():
        if child.tag == "template":
            automata_network.append(load_template(child))
        elif child.tag == "declaration":
            declaration = child.text
            #variables.extend(re.findall(r"\bvar_\w+", declaration))
            #variables.extend(re.findall(r"\binput_\w+", declaration))
            #variables.extend(re.findall(r"\boutput_\w+", declaration))
            #variables.extend(re.findall(r"\bclock_\w+", declaration))
            list_vars = re.findall(r"(\bvar_\w+|\binput_\w+|\boutput_\w+|\bclock_\w+)", declaration)
            variables = dict([(var, 0) for var in list_vars])
            consts = [(k, int(v)) for k,v in re.findall(r"(\bconst_var_\w+)\s*=\s*(\d)", declaration)]
            variables.update(consts) #re.findall(r"\bconst_var_\w+", declaration))
            #variables["var_numInputRead"] = 1
            print variables
    if automata_network:
        automata_network[0].set_variables(variables)
    return automata_network

if __name__ == '__main__':
    #result = load_system("/Users/rodrigopex/Msc/dissertacao/msc/code/doc/uppaal_examples/xsimulator_example_sync3.xml")#"/Users/rodrigopex/Msc/dissertacao/msc/code/resource/bi_copy.xml")
    #result = load_system("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/bi_copy.xml")
    #result = load_system("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/traduzido.xml")
    result = load_system("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/engarrafadora.xml")
    for i in result:
        print i
        