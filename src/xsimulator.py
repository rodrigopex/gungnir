#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
 File: xsimulator.py
 Author: rodrigopex - rodrigopex@email
 Date: Jun 21, 2010

 License: GPL
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 USA
'''
from timedwautomata import *
from timedautomatawords import TimedWWord
from state import State, Transition
import random


class Gungnir:
    def __init__(self, timewword, twautomatas):
        self.timewword = timewword
        self.twautomatas = twautomatas
    
    def filter_candidates(self):
        all_waiting_for = reduce(lambda a, b: a.union(b),
                                 [x.waiting_for for x in self.twautomatas])
        for atm in self.twautomatas:
            if atm.guards_candidate:
                if atm.wants_emit():
                    channel = list(atm.emit.intersection(all_waiting_for))
                    if channel:
                        pass
                    else:
                        atm.remove_candidates_emit_channel()
        all_emit = reduce(lambda a, b: a.union(b),
                          [x.emit for x in self.twautomatas])
        for atm in self.twautomatas:
            if atm.guards_candidate:
                if atm.is_waiting_for():
                    for channel in list(atm.waiting_for):
                        if channel in all_emit:
                            pass
                        else:
                            atm.remove_candidates_waiting_channel(channel)
                        
    def filter_candidates_2(self, channel):
        for atm in self.twautomatas:
            atm.filter_by_channel(channel)
            
    def clear_atms_candidates(self):
        for atm in self.twautomatas:
            atm.clear_guard_candidate()
                    
    def simulate(self, duration):
        for iteration in xrange(1, duration):
            
            #FASE 1 = List possible candidates to synchronize ==================
            for atm in self.twautomatas:
                atm.list_candidates()
            #END FASE 1 ========================================================
            
            #FASE 2 = Filter the possible waiting transitions. If don't exists
            #the emit transition to the needed signal this transition cannot
            #synchronizes, so it will be removed from candidates' list =========    
            self.filter_candidates()
            #END FASE 2 ========================================================
            
            #FASE 3 = Choose a candidate to synchronize. If there are no 
            #possible candidate, then a deadlock is found ======================
            chosen_atm = None
            chosen_transition = None
            
            random.shuffle(self.twautomatas)
            for chosen_atm in self.twautomatas:
                if chosen_atm.guards_candidate:
                    #print("Transition chosen is: {0} ================================".format(chosen_atm.name))
                    chosen_transition = chosen_atm.choose_candidate()
                    break
            else:
                raise Exception("DeadLock!")
            #END FASE 3 ========================================================
            
            #FASE 4 = It is necessary to filter again the candidates list, thus
            #the just the signal of the chosen transition can be synchronized ==
            if chosen_transition != None:
                self.filter_candidates_2(chosen_transition)
            else:
                self.clear_atms_candidates()
            #END FASE 4 ========================================================
            
            #FASE 5 = Execute the necessary synchronizations =================== 
            for atm in self.twautomatas:
                atm.choose_candidate()
            #END FASE 5 ========================================================
            
            
            #FASE 6 = Verify if the chosen transition is time consuming ========
                
            if chosen_atm.current_state.committed:
                #IS NOT time consuming.
                #print("COMMITTED")
                pass
            else:
                #IS time consuming.
                self.twautomatas[-1].update_clocks()
            #END ===============================================================
            if chosen_atm.name == "Updater":
                print(chosen_atm.variables)
            
def manual_test():
    s0 = State("S0")
    s1 = State("S1")
    s2 = State("S2")
    states = [s0, s1, s2]
    variables = {'va': 0, 'clock_x':0, 'clock_y':0}
    
    s0.add_transition(Transition(guards="va == 0",
                                 assignments={'clock_x': 0, 'clock_y': 0},
                                 target_state=s1,
                                 sync=None))
    s1.add_transition(Transition(guards="clock_x < 20 and va == 1",
                                 assignments={'clock_y': 0},
                                 target_state=s2,
                                 sync="a!"))
    s2.add_transition(Transition(guards="clock_y < 20 and va == 0",
                                 assignments={'clock_x': 0},
                                 target_state=s1,
                                 sync="b?"))
                
    initial_state = s0
    local_ta = TimedAutomata(states, initial_state, variables, name="Main")
#    tww = TimedWWord(zip([10, 13], [{'va':0}, {'va':1}]), ['va'])
#    local_ta.timedwword = tww

    size_of_test = 200
    times_ = [10]
    new_symbols = []#[{'va':0}]
    for i in range(size_of_test):
        times_.append(times_[-1] + random.randrange(1, 20))
        new_symbols.append({'va': i % 2})
    new_symbols.append({'va': (i + 1) % 2}) 
    #print times_
    #print new_symbols
    
    
    sN = State("SN")
    states = [sN]
    variables = {"output_test": 0}
    
    sN.add_transition(Transition(guards=None,
                                 assignments={"output_test": 0},
                                 target_state=sN,
                                 sync="a?"))
    sN.add_transition(Transition(guards=None,
                                 assignments={},
                                 target_state=sN,
                                 sync="b!"))
                
    initial_state = sN
    local_ta2 = TimedAutomata(states, initial_state, variables, name="Single")
    
    
    sM = State("SM", committed=True)
    states = [sM]
    variables = {}
    
    sM.add_transition(Transition(guards=None,
                                 assignments={'va': 0},
                                 target_state=sM,
                                 sync="a?"))
    sM.add_transition(Transition(guards=None,
                                 assignments={'va': 1, "output_test": 1},
                                 target_state=sM,
                                 sync="b?"))
                
    initial_state = sM
    local_ta3 = TimedAutomata(states, initial_state, variables, name="Spam")
    
    tww = TimedWWord()
    print local_ta.variables, local_ta2.variables, local_ta3.variables
    
    test = Gungnir(tww, [local_ta, local_ta2, local_ta3])
#    test = Gungnir(tww, [local_ta, local_ta2])

    for i in [local_ta, local_ta2, local_ta3]:
        print i
    test.simulate(100)
            

def loader_test():
    from loader import load_system
    tww = TimedWWord()
    automatas = load_system("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/traduzido.xml")
    test = Gungnir(tww, automatas)
    for i in automatas:
        print i
    test.simulate(100)

def new_model_test():
    from loader import load_system
    tww = TimedWWord()
    #automatas = load_system("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/partida_motor.xml")
    automatas = load_system("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/new_model_helix.xml")
    test = Gungnir(tww, automatas)
    for i in automatas:
        print i
    test.simulate(1000)

def model_generator():
    tww = TimedWWord()
    from modelgenerator import ModelGenerator
    mg = ModelGenerator()
    #automatas = mg.digest("partida_motor_isa52.xml")
    automatas = mg.digest("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/bottle.xml")
    automatas[0].__class__.clock_granularity = 5
    test = Gungnir(tww, automatas)
    print mg.variables
    for i in automatas:
        print i
    test.simulate(32000)

if __name__ == '__main__':
    #manual_test()
    #loader_test()
    #new_model_test()
    model_generator()
    

