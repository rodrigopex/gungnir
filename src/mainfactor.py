#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
 File: mainfactor.py
 Author: rodrigopex - rodrigopex@email
 Date: Sep 30, 2010

 License: GPL
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 USA
'''

from xml.etree import ElementTree
import re
import random

class MainFactorTable:
    def __init__(self, output_name=None):
        self.output_name = output_name
        self.values = [(0,[0]), (1,[1])]
        self.var_names = []
        self.timeout = None
        self.covered = []
    
    def __repr__(self):
        zeros = []
        ones = []
        for res, vector in self.values:
            if res:
                ones.append(vector)
            else:
                zeros.append(vector)
        zeros = "\n  ".join(str(x).strip('[').strip(']') for x in zeros)
        ones = "\n  ".join(str(x).strip('[').strip(']') for x in ones)
        return \
"""
===============
{0}
---------------
   {3}
0:
  {1}
1:
  {2}
---------------
Covered: {4}
===============
""".format(self.output_name, zeros, ones,
           self.var_names, self.covered).replace("Ellipsis", "x")
        
    
    def insert_and(self, index):
        """
        >>> test = MainFactorTable()
        >>> test.insert_and(0)
        >>> print(test.values)
        [(0, [0, Ellipsis]), (0, [Ellipsis, 0]), (1, [1, 1])]
        >>> test.insert_and(0)
        >>> print(test.values)
        [(0, [0, Ellipsis, Ellipsis]), (0, [Ellipsis, 0, Ellipsis]), (0, [Ellipsis, Ellipsis, 0]), (1, [1, 1, 1])]
        >>> test.insert_and(2)
        >>> print(test.values)
        [(0, [0, Ellipsis, Ellipsis, Ellipsis]), (0, [Ellipsis, 0, Ellipsis, Ellipsis]), (0, [Ellipsis, Ellipsis, 0, Ellipsis]), (0, [Ellipsis, Ellipsis, Ellipsis, 0]), (1, [1, 1, 1, 1])]
        """
        new_values = []
        for i, (result, vector) in enumerate(self.values):
            new_vector = vector[::]
            if vector[index] == Ellipsis:
                new_vector.insert(index, Ellipsis)
                new_values.append((result, new_vector))
            elif vector[index] == 0:
                new_vector2 = new_vector[::]
                new_vector.insert(index+1, Ellipsis)
                new_vector2.insert(index, Ellipsis)
                new_values.append((result, new_vector))
                new_values.append((result, new_vector2))
            elif vector[index] == 1:
                new_vector.insert(index, 1)
                new_values.append((result, new_vector))
        self.values = new_values
            
    def insert_or(self, index):
        """
        >>> test = MainFactorTable()
        >>> test.insert_or(0)
        >>> print(test.values)
        [(0, [0, 0]), (1, [1, Ellipsis]), (1, [Ellipsis, 1])]
        >>> test.insert_or(0)
        >>> print(test.values)
        [(0, [0, 0, 0]), (1, [1, Ellipsis, Ellipsis]), (1, [Ellipsis, 1, Ellipsis]), (1, [Ellipsis, Ellipsis, 1])]
        >>> test.insert_or(2)
        >>> print(test.values)
        [(0, [0, 0, 0, 0]), (1, [1, Ellipsis, Ellipsis, Ellipsis]), (1, [Ellipsis, 1, Ellipsis, Ellipsis]), (1, [Ellipsis, Ellipsis, 1, Ellipsis]), (1, [Ellipsis, Ellipsis, Ellipsis, 1])]
        """
        new_values = []
        for i, (result, vector) in enumerate(self.values):
            new_vector = vector[::]
            if vector[index] == Ellipsis:
                new_vector.insert(index, Ellipsis)
                new_values.append((result, new_vector))
            elif vector[index] == 1:
                new_vector2 = new_vector[::]
                new_vector.insert(index+1, Ellipsis)
                new_vector2.insert(index, Ellipsis)
                new_values.append((result, new_vector))
                new_values.append((result, new_vector2))
            elif vector[index] == 0:
                new_vector.insert(index, 0)
                new_values.append((result, new_vector))
        self.values = new_values
    def insert_not(self, index):
        """
        >>> test = MainFactorTable()
        >>> test.insert_not(0)
        >>> test.insert_not(0)
        >>> test.insert_or(0)
        >>> print(test.values)
        [(0, [0, 0]), (1, [1, Ellipsis]), (1, [Ellipsis, 1])]
        >>> test.insert_or(0)
        >>> print(test.values)
        [(0, [0, 0, 0]), (1, [1, Ellipsis, Ellipsis]), (1, [Ellipsis, 1, Ellipsis]), (1, [Ellipsis, Ellipsis, 1])]
        >>> test.insert_not(2)
        >>> print(test.values)
        [(0, [0, 0, 1]), (1, [1, Ellipsis, Ellipsis]), (1, [Ellipsis, 1, Ellipsis]), (1, [Ellipsis, Ellipsis, 0])]
        >>> test.insert_not(1)
        >>> print(test.values)
        [(0, [0, 1, 1]), (1, [1, Ellipsis, Ellipsis]), (1, [Ellipsis, 0, Ellipsis]), (1, [Ellipsis, Ellipsis, 0])]
        """
        new_values = []
        for i, (result, vector) in enumerate(self.values):
            new_vector = vector[::]
            if vector[index] == Ellipsis:
                new_values.append((result, new_vector))
            elif vector[index] == 1:
                new_vector[index] = 0
                new_values.append((result, new_vector))
            elif vector[index] == 0:
                new_vector[index] = 1
                new_values.append((result, new_vector))
        self.values = new_values
        
    def is_mainfactor_for(self, var_name, level):
        possibilities = []
        for item in self.values:
            if item[0] == level:
                index = self.var_names.index(var_name)
                if item[2][index] != Ellipsis:
                    possibilities.append(item)
                    
    def is_not_mainfactor_for(self, var_name, level):
        possibilities = []
        for item in self.values:
            if item[0] == level:
                index = self.var_names.index(var_name)
                if item[2][index] == Ellipsis:
                    possibilities.append(item)
    
    def is_mainfactor_without_feedback(self):
        possibilities = []
        for item in self.values:
            index = self.var_names.index(var_name)
            if item[2][index] == Ellipsis:
                possibilities.append(item)
                
    def value_output(self, vector):
        ret = []
        for row, item in enumerate(self.values):
            result, value = item
            for column, v in enumerate(value):
                if v != Ellipsis:
                    if vector[self.var_names[column]] != v:
                        break
            else:
                ret.append((row, result))
        return random.choice(ret) # row of table, value
                
    def match_vector(self, vector):
#        ret = []
#        print self.values
        for row, item in enumerate(self.values):
            result, value = item
            for column, v in enumerate(value):
                if v != Ellipsis:
                    if vector[self.var_names[column]] != v:
                        break
            else:
                count = 1
                if self.output_name.startswith("var_timer"):
                    if result == 0:
                        pass
                    elif vector[self.output_name] != 1:
                        count = 0
                if count:
                    self.covered[row] = self.covered[row] + 1 

            #print map(lambda a: vector.__getitem__(a), self.var_names)
            #ret.append( == value)
 #       return ret
    
    def is_full_covered(self, level=1):
        ret = True
        for row in self.covered:
            if row < level:
                ret = False
                break
        return ret
    
    def count_covered(self, level=1):
        ret = 0
        for row in self.covered:
            if row >= level:
                ret += 1
        return ret
    
    def coverable_count(self):
        return len(self.values)
                


class MainFactorExtractor:
    def __init__(self):
        self.mainfactor_tables = []
        self.current_mainfactor = None
        self.timers = []
    
    def all_var(self, vector):
        ret = True
        for exp in vector:
            if exp[0] != "var":
                ret = False
                break
        return ret
    
    def feed_mainfactor_table(self, mainfactor_table, expression_tree, start=0,end=1):
        current_root = [expression_tree]
        while not self.all_var(current_root):
            next_root = []
            for index in range(len(current_root) - 1, -1 , -1): #de traz para frente para evitar deslocamento por adicao de termo
                exp = current_root[index]
                token = exp[0]
                if token == "and":
                    mainfactor_table.insert_and(index)
                    next_root.insert(0,exp[2])
                    next_root.insert(0,exp[1])
                elif token == "or":
                    mainfactor_table.insert_or(index)
                    next_root.insert(0,exp[2])
                    next_root.insert(0,exp[1])
                elif token == "not":
                    mainfactor_table.insert_not(index)
                    next_root.insert(0,exp[1])
                elif token == "var":
                    next_root.insert(0,exp)
                else:
                    raise Exception("Bad tree! Check it out!")
            current_root = next_root[::]
        for i in current_root:
            mainfactor_table.var_names.append(i[1])
        for var in mainfactor_table.values:
            mainfactor_table.covered.append(0)
        
    def load_expression(self, exp):
        expression = ""
        for child in exp.getchildren():
            if child.tag == "ref_input":
                expression = ("var", "input_{0}".format(child.attrib["name"]))
            elif child.tag == "ref_output":
                expression = ("var","output_{0}".format(child.attrib["name"]))
            elif child.tag == "ref_feedback":
                expression = ("var","feedback_{0}".format(child.attrib["name"]))
            elif child.tag.startswith("timer"):
                var_name = "var_{0}{1}".format(child.tag, child.attrib["id"])
                expression = ("var", var_name)
                self.timers.append((var_name, child.attrib["delay"], child.getchildren()[0]))
            elif child.tag in ["and", "or"]:
                expression = (child.tag,
                              self.load_expression(child.getchildren()[0]),
                              self.load_expression(child.getchildren()[1]))
            elif child.tag == "not":
                expression = ("not", self.load_expression(child.getchildren()[0]))            
        return expression
    
    def load_rung(self, rung):
        mainfactor_table = MainFactorTable()
        expression_tree = "Empty"
        for child in rung.getchildren():
            if child.tag == "result": 
                mainfactor_table.output_name = \
                "output_{0}".format(child.getchildren()[0].attrib['name'])
            elif child.tag == "op":
                expression_tree = self.load_expression(child)
        #self.mainfactor_tables.append(mainfactor_table)
        #print self.mainfactor_tables
        #print expression_tree
        self.feed_mainfactor_table(mainfactor_table, expression_tree)
        self.mainfactor_tables.append(mainfactor_table)
        #self.rungs[output] = expression
    
    def digest(self, xmlfile):
        root = ElementTree.parse(xmlfile).getroot()
        for child in root.getchildren():
            #if child.tag in ["input", "feedback", "output"]:
            #    pass
            if child.tag == "rung":
                self.load_rung(child)
        #print self.mainfactor_tables
        for i in self.timers:
            mainfactor_table = MainFactorTable(i[0])
            mainfactor_table.timeout = i[1]
            self.feed_mainfactor_table(mainfactor_table, 
                                       self.load_expression(i[2]))
            self.mainfactor_tables.append(mainfactor_table)
        return self.mainfactor_tables
        #print self.timers
                
        #return ret

def relative_variables(mainfactor_tables, vector, output_name, row=None, value=None):
    ret = []
    for table in mainfactor_tables:
        if table.output_name == output_name:
            break
    else:
        raise Exception("There is no tables with name: {0}".format(output_name))
        table = None
    
    if value:
       pass 
    
    
    
    if row != None:
        result, _values = table.values[row]
        for index, _value in enumerate(_values):
            if _value != Ellipsis:
                var = table.var_names[index]
                if var.startswith("feedback"):
                    orign_table = var.replace("feedback", "output")
                    if orign_table != table.output_name:
                        ret.extend(relative_variables(mainfactor_tables, vector, orign_table, value=_value))                                              
                elif var.startswith("input"):
                    ret.append((var, _value))
                elif var.startswith("var_timer"):
                    ret.extend(relative_variables(mainfactor_tables, vector, var, value=_value))
    return ret
            
    
def run_doctests():
    import doctest
    result = doctest.testmod()
    if result.failed == 0:
        print("No tests failed, great!")
    
def test_bottle():
    test = MainFactorExtractor()
    mts = test.digest("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/bottle_isa.xml")
    vector = {'output_bottle': 0, 'output_TMR1': 0, 'feedback_TMR2': 1, 'feedback_M2': 1, 'feedback_TMR1': 0, 'id': 0, 'output_TMR2': 0, 'var_timer_DI02': 0, 'var_timer_DI01': 0, 'feedback_bottle': 1, 'input_PB2': 0, 'input_PB1': 0, 'clock_timer_DI02': 195, 'clock_timer_DI01': 200, 'output_SOL': 0, 'output_M1': False, 'output_M2': False, 'input_LS': 1, 'input_PE': 1}
    print vector
    for i in mts: print i

    for i in mts:
        i.match_vector(vector)
        print "{0} is full covered: {1}".format(i.output_name, i.is_full_covered())
        print i.covered
        print i.count_covered()
        print "output value:", i.value_output(vector)
        print "Relative variables", relative_variables(mts, vector, "output_M2", row=1)
        
    
    print "====================="
    for i in mts:
        i.match_vector(vector)
        print "{0} is full covered: {1}".format(i.output_name, i.is_full_covered())
        print i.covered
        print i.count_covered()
        print "output value:", i.value_output(vector)

if __name__ == '__main__':
    run_doctests()
    #test_bottle()
    test = MainFactorExtractor()
    #mts = test.digest("/Users/rodrigopex/Msc/dissertacao/msc/code/resource/timer_test01_isa.xml")
    #mts = test.digest("resource/my_bottle_isa.xml")
    mts = test.digest("resource/failanalyser01.xml")
    vector = {'var_timer_DI01':1, 'input_A': 0}
    print vector
    for i in mts: print i

#    for i in mts:
#        i.match_vector(vector)
#        print "{0} is full covered: {1}".format(i.output_name, i.is_full_covered())
#        print i.covered
#        print i.count_covered()
#        print "output value:", i.value_output(vector)
#        print "Relative variables", relative_variables(mts, vector, "output_B", row=0)
#     
#        
#    
#    print "====================="
#    vector = {'var_timer_DI01':0, 'input_A': 0}
#    for i in mts:
#        i.match_vector(vector)
#        print "{0} is full covered: {1}".format(i.output_name, i.is_full_covered())
#        print i.covered
#        print i.count_covered()
#        print "output value:", i.value_output(vector)
#    

