#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
 File: timed_automata_test.py
 Author: Rodrigo Peixoto - rodrigopex@email
 Date: January 27, 2010

 Description:
 This is the timedautomata.py unittest module.

 License: GPL
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 USA
'''
from timedwautomata import TimedAutomata
from timedautomatawords import TimedWWord
from state import State, Transition
import random
import unittest


class TestTimedAutomata(unittest.TestCase):

    def setUp(self):
        variables = {'va': None, 'clock_x':0, 'clock_y':0}
        s0 = State("S0")
        s1 = State("S1")
        s2 = State("S2")
        states = [s0, s1, s2]
        
        s0.add_transition(Transition(guards="va == 0",
                                     assignments={'clock_x': 0, 'clock_y': 0},
                                     target_state=s1,
                                     sync=None))
        s1.add_transition(Transition(guards="clock_x < 20 and va == 1",
                                     assignments={'clock_y': 0},
                                     target_state=s2,
                                     sync=None))
        s2.add_transition(Transition(guards="clock_y < 20 and va == 0",
                                     assignments={'clock_x': 0},
                                     target_state=s1,
                                     sync=None))
                    
        initial_state = s0
        self.ta = TimedAutomata(states, initial_state, variables)

    def test_recognize_01(self):
        '''
        '''
        self.ta.restore_all()
        tww = TimedWWord(zip([10, 13, 22, 38], [{'va':0}, {'va':1}, {'va':0}, {'va':1}]), ['va'])
        self.ta.timedwword = tww
        clock_state = {5: "S0", 10: "S1", 13:"S2", 22:"S1", 38:"S2"}
        for i in xrange(40):
            self.ta.update_clocks()
            self.ta.list_candidates()
            self.ta.choose_candidate()
            if i in clock_state.keys():
                self.assert_(self.ta.current_state.name == clock_state[i])
    
    def test_recognize_02(self):
        '''
        '''
        self.ta.restore_all()
        tww = TimedWWord(zip([10, 13, 22, 48, 53], [{'va':0}, {'va':1}, {'va':0}, {'va':1}, {'va':0}]), ['va'])
        self.ta.timedwword = tww
        clock_state = {5: "S0", 10: "S1", 13:"S2", 22:"S1", 48:"S1", 53:"S1"}
        for i in xrange(60):
            self.ta.update_clocks()
            self.ta.list_candidates()
            self.ta.choose_candidate()
            if i in clock_state.keys():
                self.assert_(self.ta.current_state.name == clock_state[i])
            
    def test_recognize_03(self):
        '''
        '''
        self.ta.restore_all()
        tww = TimedWWord(zip([10, 13, 22, 38, 63], [{'va':0}, {'va':1}, {'va':0}, {'va':1}, {'va':0}]), ['va'])
        self.ta.timedwword = tww
        
        clock_state = {5: "S0", 10: "S1", 13:"S2", 22:"S1", 38:"S2", 63:"S2"}
        for i in xrange(70):
            self.ta.update_clocks()
            self.ta.list_candidates()
            self.ta.choose_candidate()
            if i in clock_state.keys():
                self.assert_(self.ta.current_state.name == clock_state[i])
        
#        for i in xrange(70):
#            error = self.ta.step()
#            if error:
#                break
#        else:
#            self.fail("This timed wword is not acceptable for the TA and it recognizes!")
#        self.assert_(self.ta.is_broken())    
        
    def test_recognize_04(self):
        '''
        Bad formed timed wword
        '''
        self.ta.restore_all()
        tww = TimedWWord([(10, {'va': 0})], ['va'])
        self.ta.timedwword = tww
        
        clock_state = {1: "S0", 5: "S0", 10: "S1", 13:"S1", 22:"S1", 38:"S1", 63:"S1"}
        for i in xrange(80):
            self.ta.update_clocks()
            self.ta.list_candidates()
            self.ta.choose_candidate()
            if i in clock_state.keys():
                self.assert_(self.ta.current_state.name == clock_state[i])
        
#        for i in xrange(20):
#            error = self.ta.step()
#            self.assertFalse(error)
#        self.assertFalse(self.ta.is_broken())
#        
##        for time_, vars in tww:
##            error = self.ta.consume_symbol(vars, time_)
##            self.assertFalse(error)
##        self.assertFalse(self.ta.is_broken())
            
    def test_recognize_05(self):
        '''
        Bad formed timed wword
        '''
        self.ta.restore_all()
        tww = TimedWWord(zip([10, 13, 22], [{'va':0}, {'va':1}, {'va':1}]), ['va'])
        self.ta.timedwword = tww
        
        clock_state = {1: "S0", 5: "S0", 10: "S1", 13:"S2", 22:"S2", 38:"S2", 63:"S2"}
        for i in xrange(70):
            self.ta.update_clocks()
            self.ta.list_candidates()
            self.ta.choose_candidate()
            if i in clock_state.keys():
                self.assert_(self.ta.current_state.name == clock_state[i])
        
#        for i in xrange(70):
#            error = self.ta.step()
#            if error:
#                break
#        else:
#            self.fail("This timed wword is not acceptable for the TA and it recognizes!")
#        self.assert_(self.ta.is_broken())  
#        
##        for time_, vars in tww:
##            error = self.ta.consume_symbol(vars, time_)
##            if error:
##                break
##        else:
##            self.fail("This timed wword is not acceptable for the TA and it recognizes!")
##        self.assert_(self.ta.is_broken())
    
    def test_recognize_06(self):
        '''
        Well formed timed wword
        '''
        self.ta.restore_all()
        tww = TimedWWord(zip([10, 23], [{'va':0}, {'va':1}]), ['va'])
        self.ta.timedwword = tww
        clock_state = {1: "S0", 5: "S0", 10: "S1", 23:"S2", 38:"S2", 63:"S2"}
        for i in xrange(70):
            self.ta.update_clocks()
            self.ta.list_candidates()
            self.ta.choose_candidate()
            if i in clock_state.keys():
                self.assert_(self.ta.current_state.name == clock_state[i])
        
#        
#        for i in xrange(20):
#            error = self.ta.step()
#            self.assertFalse(error)
#        
##        for time_, vars in tww:
##            error = self.ta.consume_symbol(vars, time_)
##            self.assertFalse(error)
#
##        self.assert_(self.ta.is_final_state())
    
    def test_recognize_07(self):
        '''
        Well formed timed wword with dynamic size (size_of_test) 
        '''
#        
        variables = {'va': None, 'clock_x':0, 'clock_y':0}
        s0 = State("S0")
        s1 = State("S1")
        s2 = State("S2")
        states = [s0, s1, s2]
        
        s0.add_transition(Transition(guards="va == 0",
                                     assignments={'clock_x': 0, 'clock_y': 0},
                                     target_state=s1,
                                     sync=None))
        s1.add_transition(Transition(guards="clock_x < 20 and va == 1",
                                     assignments={'clock_y': 0},
                                     target_state=s2,
                                     sync=None))#"a!"))
        s2.add_transition(Transition(guards="clock_y < 20 and va == 0",
                                     assignments={'clock_x': 0},
                                     target_state=s1,
                                     sync=None))#"b?"))
                    
        initial_state = s0
        local_ta = TimedAutomata(states, initial_state, variables)

        size_of_test = 200
        times_ = [10]
        new_symbols = []#[{'va':0}]
        for i in range(size_of_test):
            times_.append(times_[-1] + random.randrange(1, 20))
            new_symbols.append({'va': i % 2})
        new_symbols.append({'va': (i + 1) % 2}) 
        print times_
        print new_symbols
        
        tww = TimedWWord(zip(times_, new_symbols), ['va'])
        local_ta.timedwword = tww
        print tww
        clock_state = dict(zip(times_[:], list(["S2" if x % 2 else "S1" for x in range(len(times_))])))
        for j in xrange(1, 2500):
            #print local_ta.system_clock
            local_ta.update_clocks()
            local_ta.list_candidates()
            local_ta.choose_candidate()
            if j in clock_state.keys():
                self.assert_(local_ta.current_state.name == clock_state[j])
#                print j, local_ta.current_state.name, clock_state[j]
#                if local_ta.current_state.name != clock_state[j]:
#                    print "AQU!!!!"

    def test_recognize_08(self):
        '''
        The vb is a invalid variable.
        '''
        variables = {'va': None, 'clock_x':0, 'clock_y':0}
        s0 = State("S0")
        s1 = State("S1")
        s2 = State("S2")
        states = [s0, s1, s2]
        
        s0.add_transition(Transition(guards="va == 0",
                                     assignments={'clock_x': 0, 'clock_y': 0},
                                     target_state=s1,
                                     sync=None))
        s1.add_transition(Transition(guards="clock_x < 20 and va == 1",
                                     assignments={'clock_y': 0, 'vb': 10},
                                     target_state=s2,
                                     sync="a!"))
        s2.add_transition(Transition(guards="clock_y < 20 and va == 0",
                                     assignments={'clock_x': 0},
                                     target_state=s1,
                                     sync="b?"))
                    
        initial_state = s0
        local_ta = TimedAutomata(states, initial_state, variables)
        tww = TimedWWord(zip([10, 13], [{'va':0}, {'va':1}]), ['va'])
        local_ta.timedwword = tww
        clock_state = {1: "S0", 5: "S0", 10: "S1", 23:"S2", 38:"S2", 63:"S2"}
        for i in xrange(1, 15):
            local_ta.update_clocks()
            local_ta.list_candidates()
            local_ta.choose_candidate()
            if i == 12:
                local_ta.update_clocks()
                local_ta.list_candidates()
                self.assertRaises(NameError, local_ta.choose_candidate)
                break
    

    def test_recognize_09(self):
        '''
        Bad formed automata start state is wrong
        '''
        variables = {'va': None, 'clock_x':0, 'clock_y':0}
        s0 = State("S0")
        s1 = State("S1")
        s2 = State("S2")
        states = [s0, s1, s2]
        
        s0.add_transition(Transition(guards="va == 0",
                                     assignments={'clock_x': 0, 'clock_y': 0},
                                     target_state=s1,
                                     sync=None))
        s1.add_transition(Transition(guards="clock_x < 20 and va == 1",
                                     assignments={'clock_y': 0},
                                     target_state=s2,
                                     sync="a!"))
        s2.add_transition(Transition(guards="clock_y < 20 and va == 0",
                                     assignments={'clock_x': 0},
                                     target_state=s1,
                                     sync="b?"))
                    
        initial_state = s0
        local_ta = TimedAutomata(states, initial_state, variables)
        tww = TimedWWord(zip([10, 13, 20], [{'va':0}, {'va':1}, {'va':0}]), ['va'])
        local_ta.timedwword = tww
        clock_state = {1: "S0", 5: "S0", 10: "S1", 13:"S2", 20:"S1", 63:"S2"}
        for i in xrange(1, 35):
            local_ta.update_clocks()
            local_ta.list_candidates()
            print "Emiting channels: {0} | Waiting for channels: {1}" \
                  .format(local_ta.emit, local_ta.waiting_for)
            local_ta.choose_candidate()
            if i in clock_state.keys():
                self.assert_(local_ta.current_state.name == clock_state[i])
       

    def test_recognize_10(self):
        '''
        Well formed timed wword without timers and emit ch_a! event
        '''
        s0 = State("S0")
        s1 = State("S1")
        s2 = State("S2")
        states = [s0, s1, s2]
        variables = {'v_a': None}
        s0.add_transition(Transition(guards="v_a == 0", assignments={},
                                     target_state=s1, sync=None))
        s1.add_transition(Transition(guards="v_a == 1", assignments={},
                                     target_state=s2, sync=None))
        s2.add_transition(Transition(guards="v_a == 0", assignments={},
                                     target_state=s1, sync="ch_a!"))
        initial_state = s0
        
        local_ta = TimedAutomata(states, initial_state, variables)
        
        tww = TimedWWord(zip([10, 13], [{'v_a':0}, {'v_a':1}]), ['v_a'])
        last_state = local_ta.current_state
        for time_, vars in tww:
            error = local_ta.consume_symbol(vars, time_)
            self.assertFalse(error)
            sync = local_ta.emit.pop() if local_ta.emit else None
            self.assert_(sync == "ch_a!" if last_state == "S2" else sync == None)
        
#        self.assert_(local_ta.is_final_state())
        
    def test_recognize_11(self):
        '''
        Malformed word 110101
        '''
        s0 = State("S0")
        s1 = State("S1")
        s2 = State("S2")
        s3 = State("S3")
        states = [s0, s1, s2, s3]
        s0.add_transition(Transition(guards="v_a == 0", assignments={}, target_state=s1, sync=None))
        s1.add_transitions([Transition(guards="v_a == 0", assignments={}, target_state=s2, sync=None),
                           Transition(guards="v_a == 1", assignments={}, target_state=s3, sync=None)])
        s2.add_transition(Transition(guards="v_a == 0", assignments={}, target_state=s0, sync=None))
        s3.add_transition(Transition(guards="v_a == 0", assignments={}, target_state=s0, sync=None))
        variables = {'a': None}
#        trans_func = {'S0': {'guards': "a == 1",
#                             'assignments':{},
#                             'target_state':'S1',
#                             'sync': None},
#                      'S1': [{'guards': "a == 0",
#                             'assignments':{},
#                             'target_state':'S2',
#                             'sync': None},
#                             {'guards': "a == 1",
#                             'assignments':{},
#                             'target_state':'S3',
#                             'sync': None}],
#                      'S2': {'guards': "a == 0",
#                             'assignments':{},
#                             'target_state':'S0',
#                             'sync': None},
#                      'S3': {'guards': "a == 0",
#                             'assignments':{},
#                             'target_state':'S0',
#                             'sync': None}}
        initial_state = s0
    
        local_ta = TimedAutomata(states, initial_state, variables, "TestTA_01")
        
        tww = TimedWWord([(10, {'a':1}), (20, {'a':1}), (30, {'a':0}),
                          (40, {'a':1}), (50, {'a':0}), (60, {'a':1})], ['a'])
        
        errors = []
        for time_, vars_ in tww:
            error = local_ta.consume_symbol(vars_, time_, True)
            errors.append(error)
        self.assert_(any(errors))
            
    def test_recognize_12(self):
        s0 = State("S0")
        s1 = State("S1")
        s2 = State("S2")
        s3 = State("S3")
        
        states = [s0, s1, s2, s3]
        variables = {'a': None, 'c_x': 0}
        
        s0.add_transition(Transition(guards="a == 1",
                                     assignments={},
                                     target_state=s1, sync=None))
        s1.add_transitions([Transition(guards="a == 0",
                                      assignments={'c_x':0},
                                      target_state=s2,
                                      sync=None),
                           Transition(guards="a == 1",
                                      assignments={'c_x': 10},
                                      target_state=s3, sync=None)
                                      ])
        s2.add_transition(Transition(guards="c_x < 21 and a == 0",
                                     assignments={},
                                     target_state=s0, sync=None))
        s3.add_transition(Transition(guards="c_x < 21 and a == 0",
                                     assignments={}, target_state=s0, sync=None))
#x
        initial_state = s0

    
        local_ta = TimedAutomata(states, initial_state, variables, "TestTA_01")
        
        tww = TimedWWord([(10, {'a':1}), (20, {'a':1}), (30, {'a':0}),
                          (40, {'a':1}), (50, {'a':0}), (60, {'a':0})], ['a'])
        results = []
        for time_, vars_ in tww:
            results.append(local_ta.consume_symbol(vars_, time_, "nop"))
        
        self.assert_(any(results))

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestTimedAutomata)
    unittest.TextTestRunner(descriptions=1, verbosity=2).run(suite)
