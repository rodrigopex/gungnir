#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
 File: state.py
 Author: Rodrigo Peixoto - rodrigopex@gmail.com
 Date: March 31, 2010
 
 Description:
 This module is responsible for simulating traces using a specific model
 defined in timedautomata.py module. Read its documentation for more model's
 details. The Gungnir-Simulator runs in two modes: console (or automatic) and 
 graphical (or debug). It just necessary execute the run method to activate the 
 console mode and run the run_debug to activate the graphical mode.   

 License: GPL
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 USA
'''
import types


class Transition:
    '''
    Transition
    '''
    def __init__(self, guards=None, assignments=None, target_state=None, sync=None):
        self.guards = guards
        self.assignments = assignments
        self.target_state = target_state
        self.sync = sync
        
    def __getitem__(self, name):
        try:
            return self.__dict__[name]
        except KeyError:
            raise Exception("Trying to acesse an inexistent variable {0}".format(name))
        
    def __repr__(self):
        ret = "{0}'-{1}-{4}-> {2}/{3}"
        sync = "({0})".format(self.sync) if self.sync else ""
        guards = "[{0}]".format(self.guards) if self.guards else ""
        return ret.format(" "*(3+len(self.target_state.name)),
                          sync, self.target_state.name, self.assignments, guards)
        

class State:
    '''
    State
    '''
    def __init__(self, name=None, transitions=None, committed=None):
        self.name = name or "no_name"
        self.transitions = transitions or []
        self.committed = committed 
        
    def __repr__(self):
        ret = "{0}---.{2}\n{1}"
        return ret.format(self.name,
                          "\n".join([str(trans) for trans in self.transitions]),
                          " +++ [Commited]" if self.committed else "")
        
    def add_transition(self, transition):
        if type(self.transitions) == types.ListType:
            self.transitions.append(transition)
        else:
            raise Exception("Transition is None, there was some problem!")
    
    def add_transitions(self, transitions_list):
        for trans in transitions_list:
            self.add_transition(trans)
            

if __name__ == '__main__':
    sM = State("SM")
    sM.commited = 1
    states = [sM]
    variables = {}
    
    sM.add_transition(Transition(guards="va < 10",
                                 assignments={'va': 0},
                                 target_state=sM,
                                 sync="a?"))
    sM.add_transition(Transition(guards="va > 1000",
                                 assignments={'va': 1},
                                 target_state=sM,
                                 sync="b?"))
                
    print sM
        